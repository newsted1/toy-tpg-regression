from argparse import ArgumentParser
import os
from multiprocessing.pool import ThreadPool
import random
from threading import Lock

from pmlb import fetch_data, classification_dataset_names, regression_dataset_names

from parser import parse_superconductor, parse_simple_regression
from tpg.tpg_trainer import TpgTrainer

parser = ArgumentParser()
parser.add_argument('--gens', type=int, default=100, help='Max number of generations')
parser.add_argument('--max_teams', type=int, default=100, help='Max number of teams')
parser.add_argument('--root_teams', type=int, default=0, help='Target number of root teams')
parser.add_argument('--program_size', type=int, default=20, help='Max program size')
parser.add_argument('--gap', type=float, default=0.5, help='Percent of population to replace')
parser.add_argument('--threads', type=int, default=1, help='Number of threads you want to run')
parser.add_argument('--seed', type=int, default=0, help='Random seed for trainer')
parser.add_argument('--batch_size', type=int, default=-1, help='Size of minibatches')
parser.add_argument('--dataset', type=str, default='1201_BNG_breastTumor', help='Dataset name')
parser.add_argument('--shuffle', type=bool, default=False, help='Whether to shuffle the dataset')
parser.add_argument('--valid_split', type=float, default=0.2, help='Percent to validate on')
parser.add_argument('--verbose', type=int, default=0, help='The verbosity of the output')
args = parser.parse_args()

agent_lock = Lock()
lowestError = 0
bestAgent = 0
verbose = args.verbose

def batch(iterable, n=1):
	l = len(iterable)
	for ndx in range(0,l,n):
		yield iterable[ndx:min(ndx + n, l)]

def multi_batch(i1, i2, n=1):
	if len(i1) != len(i2):
		print('Iterables must be of equal length')
		return
	l = len(i1)
	for ndx in range(0,l,n):
		yield i1[ndx:min(ndx+n,l)], i2[ndx:min(ndx+n,l)]

def train_agent(agent, agent_num, features, labels, regression):
	global agent_lock, lowestError, bestAgent, verbose
	error = 0
	for idx, x in enumerate(features):
		guess = agent.act(x)
		if not regression:
			guess = int(guess)
		error += ((labels[idx] - guess) ** 2) / len(features)
	with agent_lock:
		if error < lowestError or lowestError < 0:
			lowestError = error
			bestAgent = agent_num
	agent.reward(1 / error)
	if verbose > 0:
		print('Agent #{}, Reward: {}'.format(error), flush=True)
	
	

if __name__ == '__main__':
	data_path = os.path.join('.', 'data')
	is_pmlb = False
	regression = False
	if args.dataset in regression_dataset_names or args.dataset in classification_dataset_names:
		is_pmlb = True
		if args.dataset in regression_dataset_names:
			regression = True
		else:
			regression = False
	if is_pmlb:
		train_x, train_y = fetch_data(args.dataset, return_X_y=True, local_cache_dir=data_path)
	else:
		if args.dataset == 'superconductor':
			train_x, train_y = parse_superconductor(os.path.join(data_path, 'superconduct', 'train.csv'))
		elif args.dataset == 'simple_regression':
			train_x, train_y = parse_simple_regression(os.path.join(data_path, 'simple_regression', 'a10_b-12.csv'))
	trainer = TpgTrainer(range(2),
								randSeed=args.seed,
								teamPopSize=args.max_teams,
							 	maxProgramSize=args.program_size, 
								rTeamPopSize=args.root_teams, 
								gap=args.gap)

	max_gens = args.gens
	max_threads = args.threads
	batch_size = args.batch_size if args.batch_size > 0 else len(train_x)
	should_shuffle = args.shuffle
	valid_start = int(len(train_x) * (1 - args.valid_split))
	valid_file = open(os.path.join('.','runs','{}_valid.csv'.format(args.dataset)), 'w')
	for gen in range(max_gens):
		batch_num = 0
		if should_shuffle:
			c = list(zip(train_x, train_y))
			random.shuffle(c)
			train_x, train_y = zip(*c)
		for features, labels in multi_batch(train_x[:valid_start], train_y[:valid_start], batch_size):
			batch_num += 1
			lowestError = -1
			bestAgent = -1
			thread_pool = ThreadPool(processes=max_threads)
			while True:
				teamNum = trainer.remainingAgents()
				agent = trainer.getNextAgent()
				if agent is None:
					break
				thread_pool.apply_async(train_agent, args=(agent, teamNum, features, labels, regression))
			thread_pool.close()
			thread_pool.join()
			print('Gen #{}-{}, Best agent: {}, Error: {}'.format(gen, batch_num, bestAgent, lowestError))
			trainer.evolve()
		# Validation pass
		valid_pool = ThreadPool(processes=max_threads)
		lowestError = -1
		bestAgent = -1
		while True:
			teamNum = trainer.remainingAgents()
			agent = trainer.getNextAgent()
			if agent is None:
				break
			valid_pool.apply_async(train_agent, args=(agent, teamNum, train_x[valid_start:], train_y[valid_start:], regression))
		valid_pool.close()
		valid_pool.join()
		valid_file.write('{},{},{}\n'.format(gen, bestAgent, lowestError))
		valid_file.flush()
		print('Validation pass: Gen #{}, Best agent #{}, Lowest error: {}'.format(gen, bestAgent, lowestError))
		trainer.evolve()
	valid_file.close()
